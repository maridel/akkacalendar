
val circeVersion = "0.9.3"
val AkkaVersion = "2.6.8"
val AkkaHttpVersion = "10.2.1"

name := "akkaCalendar"
scalaVersion := "2.12.4"

resolvers += "typesafe" at "https://repo.typesafe.com/typesafe/releases/"

connectInput in run := true
libraryDependencies += "org.scalamock" %% "scalamock" % "4.4.0" % Test
libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.0" % Test
libraryDependencies += "joda-time" % "joda-time" % "2.10.8"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.2"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.2" % "test"
libraryDependencies += "com.typesafe.slick" %% "slick" % "3.3.1"
libraryDependencies += "com.h2database" % "h2" % "1.4.200"
libraryDependencies += "com.typesafe.slick" %% "slick-hikaricp" % "3.3.1"
libraryDependencies += "com.github.tototoshi" %% "slick-joda-mapper" % "2.4.2"
libraryDependencies += "org.mindrot" % "jbcrypt" % "0.4"
libraryDependencies ++= Seq(
  "org.joda" % "joda-convert" % "1.6",
  "com.bot4s" %% "telegram-akka" % "4.4.0-RC2",
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "com.bot4s" %% "telegram-core" % "4.4.0-RC2",
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,
  "com.typesafe.akka" %% "akka-actor" % AkkaVersion,
  "de.heikoseeberger" %% "akka-http-circe" % "1.20.0",
  "org.slf4j" % "slf4j-nop" % "1.6.4"
)
