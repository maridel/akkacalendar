# Календарь - планер встреч AkkaCalendar

Давно хотите встретиться с другом,
 но не можете найти день, когда вам удобно?
 В этом вам поможет AkkaCalendar!
 
Реализует 2 интерфейса пользователя:

По умолчанию запускается http server, 
для запуска бота необходимо передать параметр bot
 при запуске  
 
### Телеграм бот
- Аутентификация по id чата
- createuser - укажите после команды имя пользователя
- profile - просмотр профиля пользователя
- addevent - параметры: даты начала/конца(dd/MM/yyyy/HH:mm), название мероприятия
- getcommonfreetime - параметры: id друга и даты начала/конца периода поиска
- initgoogle - добавьте GoogleCalendar
(переход по ссылке для oauth2 и затем возвращение в бот)
- addfriend - добавьте id друга
- deletefriend - удалите друга
- deleteevent - удалите событие
- importall - импорт всех событий из GoogleCalendar

 
### Http backend на Akka
- Base http authentication(пароли хранятся захэшированными)

###### CalendarApi
- deleteevent
- deletefriend
- intersection
- addfriend
- update добавление события
- create создание пользователя(вернет id)
- userinfo просмотр профиля

###### GoogleCalendarApi
- export - экспорт текущих событий в новый календарь
- importcalendar - импорт событий конкретного календаря из GoogleCalendar
- importall - импорт всех событий из GoogleCalendar
- initgoogle - инициализация GoogleCalendar

Для того, чтобы запустить у себя, необходимо создать H2 Dattabase и 
добавить настройки в src/main/resources/application.conf

Там же хранится и токен бота.

#### Используемые технологии

- akka
- joda-time
- bot4s.telegram
- slick
- h2
- circe
- jbcrypt
____________
- scalatest
- scalamock

![image](https://gitlab.com/maridel/akkacalendar/-/blob/draft/src/main/resources/PngDiagram.png)

