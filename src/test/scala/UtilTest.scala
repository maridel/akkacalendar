import calendar.api.GoogleCalendarApi
import org.joda.time.DateTime
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import calendar.entities.Event
import calendar.logic.GoogleClient

import scala.List

class UtilTest extends AnyFlatSpec with should.Matchers{
  val client = new GoogleClient
  val test1 = """{
            |    "kind": "calendar#events",
            |    "etag": "\"p32selsssoqueq0g\"",
            |    "summary": "marideldaicher@gmail.com",
            |    "updated": "2020-12-07T18:56:51.955Z",
            |    "timeZone": "Asia/Yekaterinburg",
            |    "accessRole": "owner",
            |    "defaultReminders": [
            |        {
            |            "method": "popup",
            |            "minutes": 30
            |        }
            |    ],
            |    "nextSyncToken": "CLjq85zGvO0CELjq85zGvO0CGAU=",
            |    "items": [
            |        {
            |            "kind": "calendar#event",
            |            "etag": "\"3214733223674000\"",
            |            "id": "1bt5keq4mu3idmtp4vvpmvnhed",
            |            "status": "confirmed",
            |            "htmlLink": "https://www.google.com/calendar/event?eid=MWJ0NWtlcTRtdTNpZG10cDR2dnBtdm5oZWQgbWFyaWRlbGRhaWNoZXJAbQ",
            |            "created": "2020-12-07T18:43:31.000Z",
            |            "updated": "2020-12-07T18:43:31.837Z",
            |            "summary": "meet1",
            |            "creator": {
            |                "email": "marideldaicher@gmail.com",
            |                "self": true
            |            },
            |            "organizer": {
            |                "email": "marideldaicher@gmail.com",
            |                "self": true
            |            },
            |            "start": {
            |                "dateTime": "2020-12-01T01:00:00+05:00"
            |            },
            |            "end": {
            |                "dateTime": "2020-12-01T04:00:00+05:00"
            |            },
            |            "iCalUID": "1bt5keq4mu3idmtp4vvpmvnhed@google.com",
            |            "sequence": 0,
            |            "reminders": {
            |                "useDefault": true
            |            }
            |        },
            |        {
            |            "kind": "calendar#event",
            |            "etag": "\"3214733368866000\"",
            |            "id": "46lq1jsrojuf93fd2jk1j4g2ei",
            |            "status": "confirmed",
            |            "htmlLink": "https://www.google.com/calendar/event?eid=NDZscTFqc3JvanVmOTNmZDJqazFqNGcyZWkgbWFyaWRlbGRhaWNoZXJAbQ",
            |            "created": "2020-12-07T18:44:44.000Z",
            |            "updated": "2020-12-07T18:44:44.433Z",
            |            "summary": "meet2",
            |            "creator": {
            |                "email": "marideldaicher@gmail.com",
            |                "self": true
            |            },
            |            "organizer": {
            |                "email": "marideldaicher@gmail.com",
            |                "self": true
            |            },
            |            "start": {
            |                "dateTime": "2020-12-03T04:45:00+05:00"
            |            },
            |            "end": {
            |                "dateTime": "2020-12-03T07:15:00+05:00"
            |            },
            |            "iCalUID": "46lq1jsrojuf93fd2jk1j4g2ei@google.com",
            |            "sequence": 0,
            |            "reminders": {
            |                "useDefault": true
            |            }
            |        }
            |    ]
            |}""".stripMargin
  val test2 =
    """{
      | "kind": "calendar#calendarList",
      | "etag": "\"p32selsssoqueq0g\"",
      | "nextSyncToken": "CLjq85zGvO0CEhhtYXJpZGVsZGFpY2hlckBnbWFpbC5jb20=",
      | "items": [
      |  {
      |   "kind": "calendar#calendarListEntry",
      |   "etag": "\"1607367392686000\"",
      |   "id": "marideldaicher@gmail.com",
      |   "summary": "marideldaicher@gmail.com",
      |   "timeZone": "Asia/Yekaterinburg",
      |   "colorId": "14",
      |   "backgroundColor": "#9fe1e7",
      |   "foregroundColor": "#000000",
      |   "selected": true,
      |   "accessRole": "owner",
      |   "defaultReminders": [
      |    {
      |     "method": "popup",
      |     "minutes": 30
      |    }
      |   ],
      |   "notificationSettings": {
      |    "notifications": [
      |     {
      |      "type": "eventCreation",
      |      "method": "email"
      |     },
      |     {
      |      "type": "eventChange",
      |      "method": "email"
      |     },
      |     {
      |      "type": "eventCancellation",
      |      "method": "email"
      |     },
      |     {
      |      "type": "eventResponse",
      |      "method": "email"
      |     }
      |    ]
      |   },
      |   "primary": true,
      |   "conferenceProperties": {
      |    "allowedConferenceSolutionTypes": [
      |     "hangoutsMeet"
      |    ]
      |   }
      |  },
      |  {
      |   "kind": "calendar#calendarListEntry",
      |   "etag": "\"1607367403502000\"",
      |   "id": "addressbook#contacts@group.v.calendar.google.com",
      |   "summary": "Дни рождения",
      |   "description": "Показывает дни рождения, годовщины и другие значимые события для людей в Google Контактах.",
      |   "timeZone": "Asia/Yekaterinburg",
      |   "colorId": "13",
      |   "backgroundColor": "#92e1c0",
      |   "foregroundColor": "#000000",
      |   "accessRole": "reader",
      |   "defaultReminders": [],
      |   "conferenceProperties": {
      |    "allowedConferenceSolutionTypes": [
      |     "hangoutsMeet"
      |    ]
      |   }
      |  },
      |  {
      |   "kind": "calendar#calendarListEntry",
      |   "etag": "\"1607367403502000\"",
      |   "id": "ru.russian#holiday@group.v.calendar.google.com",
      |   "summary": "Праздники РФ",
      |   "description": "Праздники и памятные даты Российской Федерации",
      |   "timeZone": "Asia/Yekaterinburg",
      |   "colorId": "8",
      |   "backgroundColor": "#16a765",
      |   "foregroundColor": "#000000",
      |   "accessRole": "reader",
      |   "defaultReminders": [],
      |   "conferenceProperties": {
      |    "allowedConferenceSolutionTypes": [
      |     "hangoutsMeet"
      |    ]
      |   }
      |  },
      |  {
      |   "kind": "calendar#calendarListEntry",
      |   "etag": "\"1607367411955000\"",
      |   "id": "q56vjupo1s80rtdp3rsr153kgc@group.calendar.google.com",
      |   "summary": "тестовый календарь",
      |   "description": "тестовый календарь",
      |   "timeZone": "Asia/Yekaterinburg",
      |   "colorId": "22",
      |   "backgroundColor": "#f691b2",
      |   "foregroundColor": "#000000",
      |   "selected": true,
      |   "accessRole": "owner",
      |   "defaultReminders": [],
      |   "conferenceProperties": {
      |    "allowedConferenceSolutionTypes": [
      |     "hangoutsMeet"
      |    ]
      |   }
      |  }
      | ]
      |}""".stripMargin
  it should "get events from answer" in {
    val res = List(
      Event(DateTime.parse("2020-12-01T01:00:00+05:00"),
        DateTime.parse("2020-12-01T04:00:00+05:00"),"meet1"),
      Event(DateTime.parse("2020-12-03T04:45:00+05:00"),
        DateTime.parse("2020-12-03T07:15:00+05:00"),"meet2")
    )
    client.getAllEvents(test1).toList should be(res)
  }
  it should "get id from answer" in {
    val res = List(
      "marideldaicher@gmail.com",
      "addressbook#contacts@group.v.calendar.google.com",
      "ru.russian#holiday@group.v.calendar.google.com",
      "q56vjupo1s80rtdp3rsr153kgc@group.calendar.google.com"
    )
    client.getAllIds(test2).toList should be(res)
  }
}
