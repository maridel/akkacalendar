//package calendar
import calendar.entities.{User, UserInfo}
import org.joda.time.DateTime
//import cats.data.Ior.Right
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.parser.decode
import io.circe.syntax._
import io.circe.{Decoder, Encoder}

import collection.mutable.Stack
import org.scalatest._
import flatspec._
import matchers._
import org.scalatest.matchers.must.Matchers.be


class UserTest extends AnyFlatSpec with should.Matchers {
  val str =
    """{"info":{"name":"catty","token":"","refreshToken":"","tokenTime":"2020-11-30T01:07:00.000+05:00","duration":0,"password":"qwerty"},"events":[],"friends":[]}"""
  val userInfo = UserInfo("catty","","",DateTime.parse("2020-11-30T01:07:00.000+05:00"),
    0,"qwerty")
  val testUser = User(userInfo, Seq(), Seq())

  it should "serialize successfully" in {
    val json: String = testUser.asJson.noSpaces
    json should be (str)
  }

  it should "deserialize successfully" in {
    import scala.Right
    val user = decode[User](str)
    user should be (Right(testUser))
  }
}
