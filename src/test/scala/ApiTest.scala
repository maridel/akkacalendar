import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpRequest}
import akka.http.scaladsl.unmarshalling.Unmarshal
import calendar.CalendarServer
import calendar.entities.{Event, User, UserInfo}
import io.circe.syntax.EncoderOps
import org.joda.time.DateTime
import org.scalatest.BeforeAndAfterAll
import org.scalatest.funsuite.AsyncFunSuite
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import akka.util.ByteString
import calendar.logic.GoogleClient
//import calendar.CalendarHttpApp.{googleRoute, mainRoute, userStorage}
import calendar.api.{CalendarApi, GoogleCalendarApi}
import calendar.logic.{CalendarService, ICalendarService}
//import calendar.storage.UserStorageImpl
import org.scalamock.clazz.MockImpl.mock
import org.scalamock.scalatest.{AsyncMockFactory, MockFactory}

import scala.collection.concurrent.TrieMap
import scala.concurrent.duration.{Duration, DurationInt}
import scala.concurrent.{Await, ExecutionContext, Future}

class ApiTest extends AsyncFunSuite with BeforeAndAfterAll with AsyncMockFactory{
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  implicit lazy val ac: ActorSystem = ActorSystem()
  implicit lazy val ec: ExecutionContext = ac.dispatcher

  val userInfo: UserInfo = UserInfo("catty","","refresh",DateTime.parse("2020-11-30T01:07:00.000+05:00"),
    0,"qwerty")
  val user: User = User(userInfo, Seq(), Seq())

  val ev: Event = Event(DateTime.parse("2020-11-30T01:07:00.000+05:00"),
    DateTime.parse("2020-12-03T01:07:00.000+05:00"), "wow")
  val user1: User = User(userInfo, Seq(ev), Seq())

  val userService: ICalendarService = mock[ICalendarService]
  val googleClient: GoogleClient = mock[GoogleClient]

  private val mainRoute:CalendarApi  = new CalendarApi(userService)
  private val googleRoute  = new GoogleCalendarApi(userService, googleClient)

  private lazy val calendar: Future[Http.ServerBinding] =
    CalendarServer(List(mainRoute, googleRoute)).start()

  override protected def beforeAll(): Unit =
    Await.result(calendar, Duration.Inf)

  override protected def afterAll(): Unit = {
    Await.result(
      calendar.flatMap(_.terminate(10.seconds))
        .flatMap(_ => ac.terminate()),
      Duration.Inf
    )
  }

  test("create user"){
    (userService.createUser _).expects(0,user).returning(Future.successful(1.toLong)).once()
    for{
      r <- Http()
        .singleRequest(HttpRequest(
          method = HttpMethods.POST,
          uri = "http://localhost/create",
        ).withEntity(ContentTypes.`application/json`, user.asJson.toString()))
      s <- r.entity.dataBytes.runFold(ByteString(""))(_ ++ _).map(_.utf8String)
      a1 = assert(s == "1")
    } yield a1
  }

  test("get user"){
    (userService.getUser _).expects(1).returning(Future.successful(user))
    (userService.hashFunction _).expects().returning(identity)
    (userService.getUser _).expects(1).returning(Future.successful(user))
    val authorization = headers.Authorization(BasicHttpCredentials("1", "qwerty"))
    for{
      u <- Http().singleRequest(HttpRequest(
        method = HttpMethods.GET,
        uri = "http://localhost/userinfo", headers = List(authorization)))
        .flatMap(t => Unmarshal(t).to[User])
      a2 = assert(u == user)
    } yield a2
  }

  test("update events"){
    (userService.getUser _).expects(1).returning(Future.successful(user))
    (userService.hashFunction _).expects().returning(identity)
    (userService.updateEvents _).expects(1, Seq(ev)).returning(Future{})
    val authorization = headers.Authorization(BasicHttpCredentials("1", "qwerty"))
    for{
      r <- Http()
        .singleRequest(HttpRequest(
          method = HttpMethods.POST,
          uri = "http://localhost/update",
          headers = List(authorization)
        ).withEntity(ContentTypes.`application/json`, ev.asJson.toString()))
    } yield assert(1===1)
  }

  test("intersect events"){
    (userService.getUser _).expects(1).returning(Future.successful(user))
    (userService.hashFunction _).expects().returning(identity)
    (userService.getSuitableTimeList _).expects(1,Seq(3.toLong),ev).returning(Future{Seq(ev)})
    val authorization = headers.Authorization(BasicHttpCredentials("1", "qwerty"))
    for{
      r <- Http()
        .singleRequest(HttpRequest(
          method = HttpMethods.GET,
          uri = "http://localhost/intersection",
          headers = List(authorization)
        ).withEntity(ContentTypes.`application/json`,
          """[{"end":{"dateTime":"2020-12-03T01:07:00.000+05:00"},
            |"start":{"dateTime":"2020-11-30T01:07:00.000+05:00"},"summary":"wow"},[3]]""".stripMargin
        ))
    } yield assert(1===1)
  }

  test("init google"){
    (userService.getUser _).expects(1).returning(Future.successful(user))
    (userService.hashFunction _).expects().returning(identity)
    (googleClient.makeAuthorizationRequest _).expects(1, *).returning(Future.successful("someuri"))
    val authorization = headers.Authorization(BasicHttpCredentials("1", "qwerty"))
    for{
      r <- Http()
        .singleRequest(HttpRequest(
          method = HttpMethods.POST,
          uri = "http://localhost/initgoogle",
          headers = List(authorization)
        ))
    } yield assert(1===1)
  }

  test("importall google"){
    (userService.getUser _).expects(1).returning(Future.successful(user))
    (userService.hashFunction _).expects().returning(identity)
    (userService.getUser _).expects(1).returning(Future.successful(user))
    val authorization = headers.Authorization(BasicHttpCredentials("1", "qwerty"))
    try {
      for{
        r <- Http()
          .singleRequest(HttpRequest(
            method = HttpMethods.GET,
            uri = "http://localhost/importall",
            headers = List(authorization)
          ))
      } yield assert(1==1)
    }
  }
}
