
import calendar.entities.Event
//import calendar.entities.EventTest.json
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import org.joda.time.{DateTime, DateTimeConstants}
//import cats.data.Ior.Right
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.parser.decode
import io.circe.syntax._
import io.circe.{Decoder, Encoder}

import collection.mutable.Stack
import org.scalatest._
import flatspec._
import matchers._
import org.scalatest.matchers.must.Matchers.be


class EventTest extends AnyFlatSpec with should.Matchers {

  val testEvent = Event(DateTime.parse("2020-11-30T01:07:00.000+05:00"),
    DateTime.parse("2020-12-04T01:07:00.000+05:00"), "meeting")
  val littleEvent = Event(DateTime.parse("2020-12-02T01:07:00.000+05:00"),
    DateTime.parse("2020-12-03T01:07:00.000+05:00"), "meeting")

  val earlyEvent = Event(DateTime.parse("2020-11-30T01:07:00.000+05:00"),
    DateTime.parse("2020-12-03T01:07:00.000+05:00"), "meeting")
  val lateEvent = Event(DateTime.parse("2020-12-02T01:07:00.000+05:00"),
    DateTime.parse("2020-12-04T01:07:00.000+05:00"), "meeting")

  it should "serialize successfully" in {
    val json: String = testEvent.asJson.noSpaces
    val res = """{"end":{"dateTime":"2020-12-04T01:07:00.000+05:00"},"start":{"dateTime":"2020-11-30T01:07:00.000+05:00"},"summary":"meeting"}"""
    json should be (res)
  }

  it should "deserialize successfully" in {
    import scala.Right
    val json = """{"end":{"dateTime":"2020-12-04T01:07:00.000+05:00"},"start":{"dateTime":"2020-11-30T01:07:00.000+05:00"},"summary":"meeting"}"""
    val event = decode[Event](json)
    event should be (Right(testEvent))
  }

  it should "substract late from early" in {
    earlyEvent.subtract(lateEvent) should be (Seq(
      Event(DateTime.parse("2020-11-30T01:07:00.000+05:00"),
        DateTime.parse("2020-12-02T01:07:00.000+05:00"), "meeting")
    ))
  }

  it should "substract early from late" in {
    lateEvent.subtract(earlyEvent) should be (Seq(
      Event(DateTime.parse("2020-12-03T01:07:00.000+05:00"),
        DateTime.parse("2020-12-04T01:07:00.000+05:00"), "meeting")
    ))
  }

  it should "substract huge from little" in {
    littleEvent.subtract(testEvent) should be (Seq())
  }

  it should "substract little from huge" in {
    littleEvent.
      subtract(testEvent) should be
    (Seq
    (Event(DateTime.parse("2020-11-30T01:07:00.000+05:00"),
      DateTime.parse("2020-12-02T01:07:00.000+05:00"), "meeting"),
      Event(DateTime.parse("2020-12-03T01:07:00.000+05:00"),
        DateTime.parse("2020-12-04T01:07:00.000+05:00"), "meeting")
    ))
  }
}
