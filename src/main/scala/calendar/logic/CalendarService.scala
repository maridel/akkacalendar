package calendar.logic

import calendar.CalendarApp.ec1
import calendar.entities.{Event, User}
import calendar.storage.IStorage
import org.joda.time.DateTime
import java.util.UUID
import org.mindrot.jbcrypt.BCrypt

import scala.concurrent.Future

class CalendarService(val usersStorage:IStorage) extends ICalendarService {

  def getUsersFreeTime(first:User, second:User, segment:Event):Seq[Event]={
    val busyTime = first.events ++ second.events
    subtractSeq(busyTime, segment)
  }

  def getUsersListFreeTime(users:Seq[User], segment:Event):Seq[Event]={
    val busyTime = users.flatMap(u => u.events)
    subtractSeq(busyTime, segment)
  }

  def subtractSeq(toSubtract:Seq[Event], segment:Event):Seq[Event]={
    toSubtract match {
      case Seq() => Seq(segment)
      case one +: rest =>
        segment.subtract(one).flatMap(el => subtractSeq(rest, el))
    }
  }

  private val salt = "$2a$10$kwM9k7p7SOqF1ITItW1kUO"

  override def hashFunction: String => String = BCrypt.hashpw(_, salt)

  override def createUser(givenId:Long, user: User): Future[Long] ={
    val hashed = hashFunction(user.info.password)
    val id = UUID.randomUUID().getMostSignificantBits
    usersStorage
      .insert(id, user.copy(info = user.info.copy(password = hashed)))
    Future.successful(id)
  }

  override def getUser(id: Long): Future[User] = {
    usersStorage
      .find(id)
      .map{
        case Some(user) => user
        case None => throw new Exception("no user with id")
      }
  }

  def checkUser(id: Long): Future[Boolean] = {
    usersStorage
      .find(id)
      .map{
        case Some(user) => true
        case None => false
      }
  }

  override def updateToken(id:Long, newToken:String,
                           refresh:String, time:DateTime, newDuration:Int): Future[Unit] ={
    for {
      u <- getUser(id)
      _ <- usersStorage.updateUserInfo(id,
        u.info.copy(token=newToken, refreshToken = refresh, tokenTime = time, duration = newDuration))
    } yield ()
  }

  override def updateEvents(id:Long, myEvents: Seq[Event]): Future[Unit] = {
    for {
      _ <- getUser(id)
      _ <- usersStorage.updateEvents(id, myEvents)
    } yield ()
  }

  def getSuitableTime(id: Long, friendId: Long, period: Event): Future[Seq[Event]] = {
    for{
      u1 <- getUser(id)
      u2 <- getUser(friendId)
    } yield getUsersFreeTime(u1, u2, period)
  }

  override def getSuitableTimeList(id: Long, friendIds: Seq[Long], period: Event):
  Future[Seq[Event]] = {
    (for{
      users <- Future.traverse[Long, User, Seq](friendIds)(u => getUser(u))
    } yield users.forall(u => u.friends.contains(id)))
      .flatMap{
      case false => Future.failed(new Exception("you are not a friend to one of ids"))
      case true => for{
        users <- Future.traverse[Long, User, Seq](friendIds :+ id)(u => getUser(u))
      } yield getUsersListFreeTime(users, period)
    }
  }

  override def addFriend(id:Long, friendID: Long): Future[Unit] ={
    for{
      _ <- getUser(id)
      _ <- getUser(friendID)
      _ <- usersStorage.updateFriends(id, Seq(friendID))
    } yield ()
  }

  override def deleteFriend(id: Long, friendID: Long): Future[Unit] = {
    for{
      _ <- getUser(id)
      _ <- getUser(friendID)
      _ <- usersStorage.deleteFriends(id, Seq(friendID))
    } yield ()
  }

  override def deleteEvent(id: Long, summary: String): Future[Unit] = {
    for{
      _ <- getUser(id)
      _ <- usersStorage.deleteEventsByName(id, Seq(summary))
    } yield ()
  }
}

/*object CalendarServiceTest extends App{
  val now: DateTime = new DateTime().dayOfWeek().setCopy(DateTimeConstants.THURSDAY)
  val meetings1 = Seq(
    Event(now.dayOfWeek().setCopy(DateTimeConstants.TUESDAY),
      now.dayOfWeek().setCopy(DateTimeConstants.WEDNESDAY), ""),
    Event(now.dayOfWeek().setCopy(DateTimeConstants.THURSDAY),
      now.dayOfWeek().setCopy(DateTimeConstants.SATURDAY), "")
  )
  val meetings2 = Seq(
    Event(now.dayOfWeek().setCopy(DateTimeConstants.FRIDAY),
      now.dayOfWeek().setCopy(DateTimeConstants.SUNDAY), "")
  )
  val period = Event(
    now.dayOfWeek().setCopy(DateTimeConstants.MONDAY),
    now.dayOfWeek().setCopy(DateTimeConstants.SUNDAY),
    "")
  val user1 = User(1,"a", meetings1)
  val user2 = User(2,"b", meetings2)

  Console.println(period.asJson)
  //val service = new CalendarService()
  //Console.println(service.getUsersFreeTime(user1, user2, period))
}*/
