package calendar.logic

import calendar.entities.{Event, User}
import org.joda.time.DateTime

import scala.concurrent.Future


trait ICalendarService {
  def createUser(id:Long, user: User): Future[Long]
  def hashFunction: String => String
  def getUser(id:Long):Future[User]
  def updateEvents(id:Long, myEvents: Seq[Event]): Future[Unit]
  def getSuitableTimeList(id: Long, friendIds: Seq[Long], period: Event):Future[Seq[Event]]
  def updateToken(id:Long, newToken:String,
                  refresh:String, time:DateTime, newDuration:Int): Future[Unit]
  def addFriend(id:Long, friendID: Long): Future[Unit]
  def deleteFriend(id:Long, friendID: Long): Future[Unit]
  def deleteEvent(id:Long, summary: String): Future[Unit]
}
