package calendar.logic


import calendar.CalendarApp.ec1
import calendar.entities.User
import calendar.storage.IStorage

import scala.concurrent.Future

class BotCalendarService(val storage:IStorage) extends CalendarService(storage) {
  override def hashFunction: String => String = identity

  override def createUser(givenId:Long, user: User): Future[Long] ={
    storage.find(givenId).flatMap{
      case Some(user) => throw new Exception("user already exists(try command profile)")
      case None => usersStorage.insert(givenId, user).map(u => givenId)
    }
  }

  override def getUser(id: Long): Future[User] = super.getUser(id)
}
