package calendar.logic

import akka.http.scaladsl.model.HttpResponse
import calendar.entities.{Event, GoogleRefreshTokenAnswer, GoogleTokenAnswer}

import scala.concurrent.Future

trait IGoogleClient {
  def makeRefreshRequest(refreshToken:String):Future[GoogleRefreshTokenAnswer]
  def makeInsertCalendarRequest(token:String, name:String): Future[String]
  def makeInsertEventsRequest(events:Seq[Event], token:String, calendarId:String): Future[Seq[HttpResponse]]
  def makeEventsListRequest(token:String, calendarId:String): Future[Seq[Event]]
  def makeAuthorizationRequest(id:Long, redirect:String): Future[String]
  def makeTokenRequest(code:String): Future[GoogleTokenAnswer]
  def makeCalendarsListRequest(token:String): Future[Seq[String]]
}
