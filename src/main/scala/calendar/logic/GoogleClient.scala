package calendar.logic

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpCharsets, HttpMethods, HttpRequest, HttpResponse, StatusCodes}
import akka.http.scaladsl.model.headers.RawHeader
import akka.util.ByteString
import calendar.CalendarApp.ac1
import calendar.CalendarApp.ec1
import calendar.entities.{Event, GoogleRefreshTokenAnswer, GoogleTokenAnswer, InsertCalendarBody}
import io.circe.parser
import io.circe.syntax.EncoderOps
import org.joda.time.{DateTime, DateTimeZone}

import scala.concurrent.Future
import scala.util.matching.Regex

class GoogleClient extends IGoogleClient {

  def getAllIds(test:String): Iterator[String] ={
    val p: Regex = """id": "(.+)"""".r
    p.findAllIn(test).matchData.map(m=>m.group(1))
  }

  def getAllEvents(w:String): Iterator[Event] ={
    val p1:Regex = """summary": "(.+)"""".r
    val summaries = p1.findAllIn(w).matchData.map(m=>m.group(1)).drop(1)
    val p2:Regex = """dateTime": "(.+)"""".r
    val pairs = p2.findAllIn(w).matchData.map(m => DateTime.parse(m.group(1))).sliding(2,2)
    summaries.zip(pairs).map(l => Event(l._2.head, l._2(1), l._1))
  }

  def getAuthorizationUri(id:Long, redirect:String): String ={
    "https://accounts.google.com/o/oauth2/v2/auth?" +
      "scope=https%3A//www.googleapis.com/auth/calendar&" +
      "access_type=offline&" +
      "include_granted_scopes=true&" +
      "response_type=code&" +
      "state="+id.toString+"&" +
      "redirect_uri="+redirect+"&" +
      "client_id=815370083592-emo1fe2av5u4mqp0pl9fmcl09h3fujb8.apps.googleusercontent.com"
  }

  def getCalendarsListRequest(token:String): HttpRequest ={
    HttpRequest(
      uri ="https://www.googleapis.com/calendar/v3/users/me/calendarList?" +
        "key=AIzaSyBUdp7t7vS6jda9vDye-JGEysfKnLDXo-Q",
      method = HttpMethods.GET
    ).withHeaders(
      RawHeader("Authorization","Bearer "+token)
    )
  }

  def getEventsListRequest(token:String, calendarId:String): HttpRequest ={
    HttpRequest(
      uri ="https://www.googleapis.com/calendar/v3/calendars/"+calendarId+"/events?" +
        "key=AIzaSyBUdp7t7vS6jda9vDye-JGEysfKnLDXo-Q",
      method = HttpMethods.GET
    ).withHeaders(
      RawHeader("Authorization","Bearer "+token)
    )
  }

  def getCalendarInsertRequest(token:String, summary:String): HttpRequest ={
    HttpRequest(
      uri ="https://www.googleapis.com/calendar/v3/calendars?" +
        "key=AIzaSyBUdp7t7vS6jda9vDye-JGEysfKnLDXo-Q",
      method = HttpMethods.POST
    ).withHeaders(
      RawHeader("Authorization","Bearer "+token)
    ).withEntity(ContentTypes.`application/json`,
      InsertCalendarBody(summary,DateTimeZone.getDefault.toString).asJson.toString())
  }

  def getEventInsertRequest(token:String, calendarId:String, event:Event): HttpRequest ={
    HttpRequest(
      uri ="https://www.googleapis.com/calendar/v3/calendars/"+calendarId+"/events?" +
        "key=AIzaSyBUdp7t7vS6jda9vDye-JGEysfKnLDXo-Q",
      method = HttpMethods.POST
    ).withHeaders(
      RawHeader("Authorization","Bearer "+token)
    ).withEntity(ContentTypes.`application/json`,
      event.asJson.toString())
  }

  def getTokenRequest(code:String): HttpRequest ={
    HttpRequest(
      uri ="https://oauth2.googleapis.com/token",
      method = HttpMethods.POST,
      entity = akka.http.scaladsl.model
        .FormData(Map("code" -> code,
          "client_id"->"815370083592-emo1fe2av5u4mqp0pl9fmcl09h3fujb8.apps.googleusercontent.com",
          "client_secret"->"Q1h7GrmH8lbHok1Pjfm4jS-J",
          "redirect_uri"->"http://localhost/auth",
          "grant_type"->"authorization_code"))
        .toEntity(HttpCharsets.`UTF-8`)
    )
  }

  def getRefreshRequest(token:String): HttpRequest ={
    HttpRequest(
      uri ="https://oauth2.googleapis.com/token",
      method = HttpMethods.POST,
      entity = akka.http.scaladsl.model
        .FormData(Map(
          "client_id"->"815370083592-emo1fe2av5u4mqp0pl9fmcl09h3fujb8.apps.googleusercontent.com",
          "client_secret"->"Q1h7GrmH8lbHok1Pjfm4jS-J",
          "refresh_token"->token,
          "grant_type"->"refresh_token"))
        .toEntity(HttpCharsets.`UTF-8`)
    )
  }

  override def makeRefreshRequest(refreshToken:String):Future[GoogleRefreshTokenAnswer]={
    for {
      r <-Http().singleRequest(getRefreshRequest(refreshToken))
      s <- r.entity.dataBytes.runFold(ByteString(""))(_ ++ _).map(_.utf8String)
      answer  = parser.decode[GoogleRefreshTokenAnswer](s)
        .getOrElse(GoogleRefreshTokenAnswer("",0,"",""))
    } yield answer
  }

  override def makeInsertCalendarRequest(token:String, name:String): Future[String] ={
    for{
      r <- Http()
        .singleRequest(getCalendarInsertRequest(token,name))
      s <- r.entity.dataBytes.runFold(ByteString(""))(_ ++ _).map(_.utf8String)
      id = getAllIds(s).toSeq.head
    } yield id
  }

  override def makeInsertEventsRequest(events:Seq[Event], token:String, calendarId:String): Future[Seq[HttpResponse]] ={
    Future.traverse(events)(ev =>
      Http().singleRequest(getEventInsertRequest(token, calendarId, ev)))
  }

  override def makeEventsListRequest(token:String, calendarId:String): Future[Seq[Event]] ={
    for{
      r <- Http()
        .singleRequest(getEventsListRequest(token, calendarId))
      s <- r.entity.dataBytes.runFold(ByteString(""))(_ ++ _).map(_.utf8String)
      events = getAllEvents(s)
    } yield events.toSeq
  }

  override def makeAuthorizationRequest(id:Long, redirect:String): Future[String] ={
    val uri = getAuthorizationUri(id, redirect)
    Http().singleRequest(HttpRequest(uri = uri)).map(_ => uri)
  }

  override def makeTokenRequest(code:String): Future[GoogleTokenAnswer] ={
    for{
      r <-Http().singleRequest(getTokenRequest(code))
      s <- r.entity.dataBytes.runFold(ByteString(""))(_ ++ _).map(_.utf8String)
      answer = parser.decode[GoogleTokenAnswer](s).getOrElse(GoogleTokenAnswer("",0,"","",""))
    } yield answer
  }

  override def makeCalendarsListRequest(token:String): Future[Seq[String]] ={
    for{
      r <- Http()
        .singleRequest(getCalendarsListRequest(token))
      s <- r.entity.dataBytes.runFold(ByteString(""))(_ ++ _).map(_.utf8String)
      ids = getAllIds(s)
    } yield ids.toSeq
  }
}
