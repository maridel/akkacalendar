package calendar.storage

import calendar.entities.{Event, User, UserInfo}

import scala.concurrent.Future

trait IStorage {

  def find(id: Long): Future[Option[User]]

  def insert(id:Long, user: User): Future[User]

  def updateUserInfo(id:Long, info: UserInfo): Future[UserInfo]

  def updateEvents(id:Long, myEvents: Seq[Event]): Future[Unit]

  def updateFriends(id:Long, myFriends: Seq[Long]): Future[Unit]

  def deleteFriends(id:Long, myFriends: Seq[Long]): Future[Unit]

  def deleteEventsByName(id:Long, Events: Seq[String]): Future[Unit]

  def close(): Unit

}

