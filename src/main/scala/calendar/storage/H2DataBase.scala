package calendar.storage

import calendar.entities.{Event, User, UserInfo}
import org.joda.time.DateTime
import slick.jdbc.H2Profile.api._

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import com.github.tototoshi.slick.H2JodaSupport._

class UsersTable(tag: Tag) extends Table[(Long, String,
  String, String, DateTime, Int, String)](tag, "USERS") {
  def id = column[Long]("ID")
  def name = column[String]("NAME")
  def token = column[String]("TOKEN")
  def refresh = column[String]("REFRESH")
  def datetime = column[DateTime]("TIME")
  def duration = column[Int]("DURATION")
  def password = column[String]("PASSWORD")
  def * = (id, name,
    token, refresh, datetime, duration, password)
}

class EventTable(tag: Tag) extends Table[(Long, DateTime, DateTime, String)](tag, "EVENTS") {
  def id = column[Long]("ID")
  def start = column[DateTime]("EVENTSTART")
  def end = column[DateTime]("EVENTEND")
  def name = column[String]("NAME")
  def *  = (id, start, end, name)
}

class FriendsTable(tag: Tag) extends Table[(Long, Long)](tag, "FRIENDS"){
  def mainId = column[Long]("MAINID")
  def friendId = column[Long]("FRIENDID")
  def *  = (mainId, friendId)
}

class H2DataBase extends IStorage{

  val db = Database.forConfig("h2db")
  val events = TableQuery[EventTable]
  val users = TableQuery[UsersTable]
  val friends = TableQuery[FriendsTable]


  override def find(id: Long): Future[Option[User]] = {
    val res = for {
      event <- events
        .filter(e => e.id === id)
        .result
      friend <- friends
        .filter(e => e.mainId === id)
        .result
      u <- users
        .filter(u => u.id===id).result.headOption
        .map{
          case Some(user) =>
            Some(User(
              UserInfo(user._2, user._3, user._4, user._5, user._6, user._7),
              event.map(e => Event(e._2, e._3, e._4)),
              friend.map(f => f._2)))
          case None => None
        }
    } yield u
    db.run(res)
  }

  override def insert(id:Long, user: User): Future[User] = {
    val evToUpdate = user.events.map(e => (id, e.period.start, e.period.end, e.summary))
    val frToUpdate = user.friends.map(e => (id, e))
    val info = user.info
    val add = DBIO.seq(
      users += (id, info.name, info.token, info.refreshToken, info.tokenTime, info.duration, info.password),
      events ++= evToUpdate,
      friends ++= frToUpdate
    )
    for{
      _ <- db.run(add)
    }yield user
  }

  override def updateUserInfo(id:Long, info: UserInfo): Future[UserInfo] = {
    val res = for {
      _ <- users
        .filter(u => u.id===id)
        .update(id, info.name, info.token, info.refreshToken, info.tokenTime, info.duration, info.password)
    } yield info
    db.run(res)
  }

  def updateEvent(id:Long, event: Event): Future[Unit] = {
    val add = DBIO.seq(events += (id, event.period.start, event.period.end, event.summary))
    db.run(add)
  }

  override def updateEvents(id:Long, myEvents: Seq[Event]): Future[Unit] = {
    val add = DBIO.seq(events ++=
      myEvents.map(event => (id, event.period.start, event.period.end, event.summary)))
    db.run(add)
  }

  def updateFriend(id:Long, friend: Long): Future[Unit] = {
    val add = DBIO.seq(friends += (id, friend))
    db.run(add)
  }

  def updateFriends(id:Long, myFriends: Seq[Long]): Future[Unit] = {
    val add = DBIO.seq(friends ++= myFriends.map(fr => (id, fr)))
    db.run(add)
  }

  def close(): Unit ={
    db.close()
  }

  override def deleteFriends(id: Long, myFriends: Seq[Long]): Future[Unit] = {
    val q = friends.filter(f => f.mainId===id && f.friendId.inSet(myFriends))
    db.run(q.delete).flatMap(f => Future.successful())
  }

  override def deleteEventsByName(id: Long, myEvents: Seq[String]): Future[Unit] = {
    val q = events.filter(ev => ev.name.inSet(myEvents))
    db.run(q.delete).flatMap(f => Future.successful())
  }
}

