package calendar

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{_symbol2NR, handleExceptions, parameters}
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.http.scaladsl.server.{Route}
import calendar.CalendarApp.ac1
import calendar.api.CalendarExceptionHandler
import com.bot4s.telegram.api.declarative.Commands
import com.softwaremill.sttp.SttpBackend
import com.softwaremill.sttp.okhttp._
import cats.instances.future._
import cats.syntax.functor._
import com.bot4s.telegram.future.Polling
import com.bot4s.telegram.methods._
import com.bot4s.telegram.api.RequestHandler
import com.bot4s.telegram.clients.FutureSttpClient
import com.bot4s.telegram.future.TelegramBot
import io.circe.syntax._
import calendar.entities.{User, UserInfo}
import calendar.logic.{GoogleClient, ICalendarService}
import org.joda.time.{DateTime, Seconds}

import scala.concurrent.{Await, Future}
import org.joda.time.format.DateTimeFormat

import scala.concurrent.duration.{DurationInt}

object SttpBackends {
  val default: SttpBackend[Future, Nothing] = OkHttpFutureBackend()
}

class CalendarBot(val token:String,
                  val service:ICalendarService,
                  val googleClient: GoogleClient) extends TelegramBot
  with Polling
  with Commands[Future] {

  implicit val backend: SttpBackend[Future, Nothing] = SttpBackends.default
  override val client: RequestHandler[Future] = new FutureSttpClient(token)

  def checkAndRefresh(id:Long): Future[Unit] ={
    service
      .getUser(id)
      .flatMap(u =>
        if (Seconds.secondsBetween(u.info.tokenTime, new DateTime()).getSeconds>=u.info.duration){
          for {
            answer <- googleClient.makeRefreshRequest(u.info.refreshToken)
            _ <- service.updateToken(id, answer.access_token, u.info.refreshToken,
              new DateTime(),answer.expires_in)
          } yield ()
        }else{
          Future.successful()
        }
      )
  }

  onCommand("createuser"){
    implicit  msg =>
        withArgs{
          args =>
            if (args.isEmpty)
              reply("invalid request:( correct format is /createuser [NAME]").void
            else{
                val id = msg.chat.id
                val name = args.head
                val user = User(UserInfo(name, "", "", new DateTime(), 0, ""), Seq(), Seq())
                service.createUser(id, user)
                  .flatMap(id => reply(id.toString).void)
                  .recoverWith{ case e:Exception =>reply(e.getMessage).void}
            }
        }
  }

  onCommand("profile"){
    implicit  msg =>
      val id = msg.chat.id
      (for{
        u <- service.getUser(id)
        r <- reply(u.asJson.toString).void
      } yield r).recoverWith{case e:Exception=>reply(e.getMessage).void}
  }

  onCommand("addevent"){
    implicit  msg =>
      withArgs{
        args =>
          if (args.length<=2)
            reply("invalid request:( " +
              "correct format is /addevent [STARTTIME] " +
              "[ENDTIME] [NAME]").void
          else{
              val id = msg.chat.id
              val start = args.head
              val end = args(1)
              val formatter = DateTimeFormat.forPattern("dd/MM/yyyy/HH:mm")
              val startdt = formatter.parseDateTime(start)
              val enddt = formatter.parseDateTime(end)
              val name = args.last
              try {
                val event = calendar.entities.Event(startdt, enddt, name)
                service.updateEvents(id, Seq(event))
                  .flatMap(ev => reply("you successfully added event").void)
                  .recoverWith{case e:Exception=>reply(e.getMessage).void}
              }catch {
                case exception: Exception => reply(exception.getMessage).void
              }

          }
      }
  }

  onCommand("getcommonfreetime"){
    implicit  msg =>
      withArgs{
        args =>
          if (args.length<=2)
            reply("invalid request:( " +
              "correct format is /getcommonfreetime [friendID] [STARTTIME] " +
              "[ENDTIME]").void
          else{
              val myid = msg.chat.id
              val id = args.head.toLong
              val start = args(1)
              val end = args.last
              val formatter = DateTimeFormat.forPattern("dd/MM/yyyy/HH:mm")
              val startdt = formatter.parseDateTime(start)
              val enddt = formatter.parseDateTime(end)
              val event = calendar.entities.Event(startdt, enddt, "")
              service.getSuitableTimeList(myid, List(id),event)
                .flatMap(ev => reply(ev.asJson.toString).void)
                .recoverWith{case e:Exception=>reply(e.getMessage).void}
          }
      }
  }

  onCommand("addfriend"){
    implicit  msg =>
      withArgs{
        args =>
          if (args.isEmpty)
            reply("invalid request:( " +
              "correct format is /addfriend [friendID]").void
          else{
            val myid = msg.chat.id
            val id = args.head.toLong
            service.addFriend(myid, id)
              .flatMap(ev => reply(ev.toString).void)
              .recoverWith{case e:Exception=>reply(e.getMessage).void}
          }
      }
  }

  onCommand("deletefriend"){
    implicit  msg =>
      withArgs{
        args =>
          if (args.isEmpty)
            reply("invalid request:( " +
              "correct format is /deletefriend [friendID]").void
          else{
            val myid = msg.chat.id
            val id = args.head.toLong
            service.deleteFriend(myid, id)
              .flatMap(ev => reply("you successfully deleted "+id.toString).void)
              .recoverWith{case e:Exception=>reply(e.getMessage).void}
          }
      }
  }

  onCommand("deleteevent"){
    implicit  msg =>
      withArgs{
        args =>
          if (args.isEmpty)
            reply("invalid request:( " +
              "correct format is /deleteevents [eventName]").void
          else{
            val myid = msg.chat.id
            val name = args.head
            service.deleteEvent(myid, name)
              .flatMap(ev => reply("you successfully deleted "+name).void)
              .recoverWith{case e:Exception=>reply(e.getMessage).void}
          }
      }
  }

  val routes: Route = handleExceptions(CalendarExceptionHandler.exceptionHandler)(
    (path("auth") & parameters("code")
      & parameters('state.as[Long])) {
      (code, id) =>
        context=>
          for{
            answer <- googleClient.makeTokenRequest(code)
            _ <- service.updateToken(id, answer.access_token, answer.refresh_token,
              new DateTime(),answer.expires_in)
            res <- context.redirect("https://t.me/AkkaCalendarBot", StatusCodes.PermanentRedirect)
            a = Await.result(binding, 2.seconds)
              .terminate(hardDeadline = 3.seconds)
          } yield res

    }
  )

  def start(): Future[Http.ServerBinding] =
    Http()
      .newServerAt("localhost", 80)
      .bind(routes)

  lazy val binding:Future[Http.ServerBinding] = start()

  onCommand("initgoogle"){
    implicit msg =>
      val b = binding
      val myid = msg.chat.id
      for{
        uri <- googleClient.makeAuthorizationRequest(myid, "http%3A//localhost/auth")
        res <- reply("<a href=\""+uri+"\">please give app access</a>",
          parseMode = Some(ParseMode.HTML)).void
      } yield res
  }

  onCommand("importall"){
    implicit msg =>
      val b = binding
      val id = msg.chat.id
      for {
        _ <- checkAndRefresh(id)
        user <- service.getUser(id)
        ids <- googleClient.makeCalendarsListRequest(user.info.token)
        _ <- Future.traverse(ids)(cId =>
          for{
            eventsSeq <- googleClient.makeEventsListRequest(user.info.token, cId)
            _ <- service.updateEvents(id, eventsSeq)
          } yield ()
        )
      } yield reply("you successfully imported all calendars")
  }

}
