package calendar


import akka.actor.ActorSystem
import calendar.api.{CalendarApi, GoogleCalendarApi}
import calendar.logic.{BotCalendarService, CalendarService, GoogleClient}
import calendar.storage.H2DataBase
import com.typesafe.config.ConfigFactory
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

object CalendarApp{
  implicit val ac1: ActorSystem = ActorSystem()
  implicit val ec1: ExecutionContext = ac1.dispatcher

  def main(args: Array[String]): Unit = {
    if (args.isEmpty){
      val userStorage = new H2DataBase
      val userService = new CalendarService(userStorage)
      val mainRoute:CalendarApi  = new CalendarApi(userService)
      val googleRoute  = new GoogleCalendarApi(userService, new GoogleClient())
      Await.result(CalendarServer(List(mainRoute, googleRoute)).start(), Duration.Inf)
      println("server is running")
    }else if (args(0)=="bot"){
      val cfg = ConfigFactory.load()
      val token = cfg.getString("tg.token")
      val userStorage = new H2DataBase
      val service = new BotCalendarService(userStorage)
      val bot = new CalendarBot(token, service, new GoogleClient())
      val eol = bot.run()
      println("bot is runnig")
      scala.io.StdIn.readLine()
      bot.shutdown()
      Await.result(eol, Duration.Inf)
    }else{
      println("invalid args")
    }

  }


  /**/
}
