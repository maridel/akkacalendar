package calendar.entities

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.joda.time.DateTime


case class User(info:UserInfo, events:Seq[Event], friends:Seq[Long])

object User {

  implicit val jsonDecoder: Decoder[User] = deriveDecoder
  implicit val jsonEncoder: Encoder[User] = deriveEncoder
}

case class UserInfo(name:String, token:String, refreshToken:String,
                    tokenTime:DateTime, duration:Int, password:String)

object UserInfo {
  import calendar.entities.JodaDateTime.decodeDateTime
  import calendar.entities.JodaDateTime.datetimeEncoder
  implicit val jsonDecoder: Decoder[UserInfo] = deriveDecoder
  implicit val jsonEncoder: Encoder[UserInfo] = deriveEncoder
}
