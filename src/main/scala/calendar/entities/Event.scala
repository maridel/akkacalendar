package calendar.entities

import org.joda.time.DateTime
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.syntax._
import io.circe.{Decoder, Encoder, HCursor, Json}

import scala.util.control.NonFatal


object JodaDateTime{
  implicit val datetimeEncoder: Encoder[DateTime] = new Encoder[DateTime] {
    final def apply(datetime: DateTime): Json = Json.fromString(datetime.toString())
  }

  implicit val decodeDateTime: Decoder[DateTime] = Decoder.decodeString.emap { s =>
    try {
      Right(DateTime.parse(s))
    } catch {
      case NonFatal(e) => Left(e.getMessage)
    }
  }
}


case class Period(start:DateTime, end:DateTime)

object Period{
  import calendar.entities.JodaDateTime.datetimeEncoder
  import calendar.entities.JodaDateTime.decodeDateTime

  implicit val jsonDecoder: Decoder[Period] = deriveDecoder
  implicit val jsonEncoder: Encoder[Period] = deriveEncoder
}

case class Event(period:Period, summary:String){
  def subtract(event: Event): Seq[Event] ={
    val a = period.start
    val b = period.end
    val c = event.period.start
    val d = event.period.end
    if (a.isBefore(c) && d.isBefore(b)){
      Seq(Event(a, c,  summary),
        Event(d, b, summary))
    }
    else if
    ((c.isBefore(a) || c.isEqual(a)) && (b.isBefore(d) || b.isEqual(d))){
      Seq()
    }else if
    (a.isBefore(c) && (c.isBefore(b) || c.isEqual(b)) && (b.isBefore(d) || b.isEqual(d))){
      Seq(Event(a, c, summary))
    }else if
    ((c.isBefore(a) || c.isEqual(a)) && (a.isBefore(d)|| a.isEqual(d)) && d.isBefore(b)){
      Seq(Event(d, b, summary))
    }else{
      Seq(Event(a, b, summary))
    }
  }
}

object Event {
  import calendar.entities.JodaDateTime.datetimeEncoder
  import calendar.entities.JodaDateTime.decodeDateTime

  def apply(start: DateTime, end: DateTime, summary: String): Event = {
    if (start.isAfter(end))
      throw new Exception("start should be earlier than end")
    new Event(Period(start, end), summary)
  }

  implicit val jsonDecoder: Decoder[Event] = new Decoder[Event] {
    final def apply(c: HCursor): Decoder.Result[Event] =
      for {
        end <- c.downField("end").downField("dateTime").as[DateTime]
        start <- c.downField("start").downField("dateTime").as[DateTime]
        summary <- c.downField("summary").as[String]
      } yield {
        Event(start, end, summary)
      }
  }

  implicit val jsonEncoder: Encoder[Event] = new Encoder[Event] {
    final def apply(event: Event): Json = {
      Json.obj(
        ("end", Json.obj(("dateTime",event.period.`end`.asJson))),
        ("start", Json.obj(("dateTime", event.period.start.asJson))),
        ("summary", Json.fromString(event.summary))
      )
    }
  }
}
