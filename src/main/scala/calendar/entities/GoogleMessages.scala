package calendar.entities

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class GoogleTokenAnswer(access_token:String, expires_in:Int,
                             token_type:String, scope:String, refresh_token:String)

object GoogleTokenAnswer{
  implicit val googleDecoder: Decoder[GoogleTokenAnswer] = deriveDecoder
  implicit val googleEncoder: Encoder[GoogleTokenAnswer] = deriveEncoder
}

case class GoogleRefreshTokenAnswer(access_token:String, expires_in:Int,
                              scope:String, token_type:String)

object GoogleRefreshTokenAnswer{
  implicit val googleDecoder: Decoder[GoogleRefreshTokenAnswer] = deriveDecoder
  implicit val googleEncoder: Encoder[GoogleRefreshTokenAnswer] = deriveEncoder
}

case class InsertCalendarBody(summary:String, timeZone:String)

object InsertCalendarBody{
  implicit val insertDecoder: Decoder[InsertCalendarBody] = deriveDecoder
  implicit val insertEncoder: Encoder[InsertCalendarBody] = deriveEncoder
}