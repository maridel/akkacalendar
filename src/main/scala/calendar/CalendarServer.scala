package calendar

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives.handleExceptions
import akka.http.scaladsl.server.{Route, RouteConcatenation}
import calendar.api.{Api, CalendarExceptionHandler}

import scala.concurrent.{ExecutionContext, Future}

case class CalendarServer(apis:Seq[Api])(implicit ac: ActorSystem, ec: ExecutionContext){

  val routes: Route = handleExceptions(CalendarExceptionHandler.exceptionHandler)(
    RouteConcatenation.concat(apis.map(_.route): _*)
  )

  def start(): Future[Http.ServerBinding] =
    Http()
      .newServerAt("localhost", 80)
      .bind(routes)
}