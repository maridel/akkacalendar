package calendar.api

import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives.{_symbol2NR, as, authenticateBasicAsync, entity, parameters, post, redirect}
import akka.http.scaladsl.server.RouteResult.Complete
import akka.http.scaladsl.server.directives.Credentials
import calendar.entities.{Event, User}
import calendar.logic.ICalendarService
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import io.circe.syntax._

class Authenticator(val calendarService: ICalendarService){
  def myUserPassAuthenticator(credentials: Credentials): Future[Option[Long]] =
    credentials match {
      case p @ Credentials.Provided(id) =>
        calendarService
          .getUser(id.toLong)
          .map(u =>
            if (p.verify(u.info.password, calendarService.hashFunction)){
              Some(id.toLong)
            }else{
              None
            })
      case _ => Future{None}
    }
}


class CalendarApi(val calendarService: ICalendarService) extends Api{
  val auth = new Authenticator(calendarService)

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
  val route: Route = {
    Directives.concat(
      (post & path("deleteevent")
        & parameters("eventsummary")
        & authenticateBasicAsync(realm = "calendar protected", auth.myUserPassAuthenticator)) {
        (summary, id) => {
          context =>{
            for{
              _ <- calendarService.deleteEvent(id,summary)
              res <- Future[RouteResult]{Complete(HttpResponse(StatusCodes.OK))}
            } yield res
          }
        }
      },
      (post & path("deletefriend")
        & parameters('friendId.as[Long])
        & authenticateBasicAsync(realm = "calendar protected", auth.myUserPassAuthenticator)) {
        (fId, id) => {
          context =>{
            for{
              _ <- calendarService.deleteFriend(id,fId)
              res <- Future[RouteResult]{Complete(HttpResponse(StatusCodes.OK))}
            } yield res
          }
        }
      },
      (Directives.get & path("intersection")
        & entity(as[(Event, Seq[Long])])
        & authenticateBasicAsync(realm = "calendar protected", auth.myUserPassAuthenticator)) {
        (pair, id1) => {
          context =>{
            for{
              u <- calendarService.getSuitableTimeList(id1, pair._2, pair._1)
              res <- Future[RouteResult]{Complete(
                HttpResponse(entity = HttpEntity(ContentTypes.`application/json`,
                  u.asJson.toString()))
              )}
            } yield res
          }
        }
      },
      (post & path("addfriend") & parameters('fid.as[Long])
        & authenticateBasicAsync(realm = "calendar protected", auth.myUserPassAuthenticator)){
        (fid, id) => {
          context =>{
            for{
              _ <- calendarService.addFriend(id, fid)
              res <- Future[RouteResult]{Complete(HttpResponse(StatusCodes.OK))}
            } yield res
          }
        }
      },
      (post & path("update") & entity(as[Event])
        & authenticateBasicAsync(realm = "calendar protected", auth.myUserPassAuthenticator)){
        (event, id) => {
          context =>{
            for{
              _ <- calendarService.updateEvents(id, Seq(event))
              res <- Future[RouteResult]{Complete(HttpResponse(StatusCodes.OK))}
            } yield res
          }
        }
      },
       (post & path("create") & entity(as[User])){
         user=> {
              context =>{
                for{
                  id <- calendarService.createUser(0.toLong, user)
                  res <- Future[RouteResult]{Complete(HttpResponse(
                    entity = HttpEntity(id.toString)
                  ))}
                } yield res
            }
          }
       },
      (Directives.get & path("userinfo")
        & authenticateBasicAsync(realm = "calendar protected", auth.myUserPassAuthenticator) ){
        id =>{
          context =>
            for {
             user <- calendarService.getUser(id)
             res <- Future[RouteResult]{Complete(
               HttpResponse(entity = HttpEntity(ContentTypes.`application/json`,
                 user.asJson.toString()))
             )}
            } yield res
        }
      }
    )
  }
}
