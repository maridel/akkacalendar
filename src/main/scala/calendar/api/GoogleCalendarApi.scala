package calendar.api


import akka.http.scaladsl.model.{ HttpResponse, StatusCodes, headers}
import akka.http.scaladsl.server.Directives.{_symbol2NR, authenticateBasicAsync, parameters, post}
import akka.http.scaladsl.server.RouteResult.Complete
import akka.http.scaladsl.server.{Directives, Route}
import akka.http.scaladsl.server.directives.PathDirectives.path
import calendar.logic.{ICalendarService, IGoogleClient}
import org.joda.time.Seconds
import org.joda.time.DateTime

import scala.concurrent.{Future}
import calendar.CalendarApp.ec1

class GoogleCalendarApi(val calendarService: ICalendarService,
                        val googleClient: IGoogleClient) extends Api{
  val auth = new Authenticator(calendarService)

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  def checkAndRefresh(id:Long): Future[Unit] ={
    calendarService
      .getUser(id)
      .flatMap(u =>
        if (Seconds.secondsBetween(u.info.tokenTime, new DateTime()).getSeconds>=u.info.duration){
          for {
            answer <- googleClient.makeRefreshRequest(u.info.refreshToken)
            _ <- calendarService.updateToken(id, answer.access_token, u.info.refreshToken,
                    new DateTime(),answer.expires_in)
          } yield ()
        }else{
          Future.successful()
        }
      )
  }

  val route: Route = {
    Directives.concat(
      (Directives.post & path("export") & parameters("name")
        & authenticateBasicAsync(realm = "calendar protected", auth.myUserPassAuthenticator)){
        (name,id) =>
        {
          context =>
            for {
              _ <-checkAndRefresh(id)
              user <- calendarService.getUser(id)
              id <- googleClient.makeInsertCalendarRequest(user.info.token,name)
              _ <- googleClient.makeInsertEventsRequest(user.events, user.info.token,id)
            } yield Complete(HttpResponse(StatusCodes.OK))
        }
      },
      (Directives.get & path("importcalendar") & parameters("calendarId")
        & authenticateBasicAsync(realm = "calendar protected", auth.myUserPassAuthenticator)){
        (cId,id) =>
        {
          context =>
            for {
              _ <- checkAndRefresh(id)
              user <- calendarService.getUser(id)
              eventsSeq <- googleClient.makeEventsListRequest(user.info.token, cId)
              _ <- calendarService.updateEvents(id, eventsSeq)
            } yield Complete(HttpResponse(StatusCodes.OK))
        }
      },
      (Directives.get & path("importall")
        & authenticateBasicAsync(realm = "calendar protected", auth.myUserPassAuthenticator)){
        id =>
          {
            context =>
              for {
                _ <- checkAndRefresh(id)
                user <- calendarService.getUser(id)
                ids <- googleClient.makeCalendarsListRequest(user.info.token)
                _ <- Future.traverse(ids)(cId =>
                  for{
                    eventsSeq <- googleClient.makeEventsListRequest(user.info.token, cId)
                    _ <- calendarService.updateEvents(id, eventsSeq)
                  } yield ()
                )
              } yield Complete(HttpResponse(StatusCodes.OK))
          }
      },
      (post & path("initgoogle")
        & authenticateBasicAsync(realm = "calendar protected", auth.myUserPassAuthenticator)){
            id =>{
              context =>
              {
                for {
                  uri <- googleClient.makeAuthorizationRequest(id,"http%3A//localhost/auth")
                  res <- context.redirect(uri, StatusCodes.PermanentRedirect)
                } yield res
              }
            }
      },
      (path("auth") & parameters("code")
        & parameters('state.as[Long])) {
        (code, id) =>
          context=>
            for{
              answer <- googleClient.makeTokenRequest(code)
              _ <- calendarService.updateToken(id, answer.access_token, answer.refresh_token,
                  new DateTime(),answer.expires_in)
            } yield Complete(HttpResponse(StatusCodes.OK))
      }
    )
  }
}
