package calendar.api

import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.server.Directives.complete
import akka.http.scaladsl.server.ExceptionHandler
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class ExceptionResponse(errorMessage: String)
object ExceptionResponse {
  implicit val jsonEncoder: Encoder[ExceptionResponse] = deriveEncoder
  implicit val jsonDecoder: Decoder[ExceptionResponse] = deriveDecoder
}


object CalendarExceptionHandler {
    import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

    val exceptionHandler: ExceptionHandler =
      ExceptionHandler {
        case e: Exception => complete(BadRequest, ExceptionResponse(e.getMessage))
      }
}
